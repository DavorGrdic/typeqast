﻿using System;
using System.Linq;
using ShoppingCart.Models;
using ShoppingCart.Processors;

namespace ShoppingCart
{
    /// <summary>
    /// Represents a shopping cart.
    /// </summary>
    public class Cart : IOrderReader, IOrderCreator
    {
        #region Properties
        public IOrder Order { get; set; }

        public virtual int ItemsInOrder => Order.Items.Sum(i => i.Quantity);

        public decimal Total
        {
            get
            {
                TotalRequested?.Invoke(this, new OrderEventArgs(Order));
                return Order.Total;
            }
        }

        public IDiscountStore DiscountStore { get; set; }
        #endregion

        #region Events
        public event EventHandler<OrderEventArgs> TotalRequested;
        public event EventHandler<OrderItemEventArgs> ProductAdded;
        public event EventHandler<OrderItemEventArgs> ProductModified;
        #endregion

        #region Methods
        public IOrderCreator Add(IProduct product)
        {
            return Add(product, 1);
        }

        public IOrderCreator Add(IProduct product, int quantity)
        {
            if (quantity <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(quantity));
            }

            IOrderItem item = Order.Items.SingleOrDefault(i => i.ID == product.OrThrow().ID);
            if (item != null)
            {
                item.Quantity += quantity;
                ProductModified?.Invoke(this, new OrderItemEventArgs(item));
            }
            else
            {
                item = new OrderItem(Order, product, quantity);
                Order.Items.Add(item);
                ProductAdded?.Invoke(this, new OrderItemEventArgs(item));
            }
            return this;
        }
        #endregion
    }
}
