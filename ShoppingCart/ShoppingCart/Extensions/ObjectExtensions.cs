﻿namespace System
{
    internal static class ObjectExtensions
    {
        internal static T OrThrow<T>(this T obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException();
            }
            return obj;
        }
    }
}
