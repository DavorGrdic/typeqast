﻿using System;
using System.Collections.Generic;
using System.Text;
using ShoppingCart.Discounts.Rules;
using ShoppingCart.Models;

namespace ShoppingCart.Discounts
{
    public abstract class Discount<T> : IDiscount where T : IOrderable
    {
        #region Properties
        public int ID { get; private set; }
        public string Name { get; set; }
        public Func<T, bool> Condition { get; private set; }
        public IDiscountApplicationRule ApplicationRule { get; set; }
        #endregion

        #region Constructors
        public Discount(int id, string name)
        {
            ID = id;
            Name = name;
        }
        #endregion

        public abstract decimal ApplyTo(IOrder order);
    }
}
