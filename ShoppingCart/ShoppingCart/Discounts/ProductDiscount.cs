﻿using System;
using System.Collections.Generic;
using System.Text;
using ShoppingCart.Discounts.Rules;
using ShoppingCart.Models;

namespace ShoppingCart.Discounts
{
    public abstract class ProductDiscount : Discount
    {
        public ProductDiscount(int id, string name, Func<IOrderItem, bool> condition) : base(id, name)
        {
        }

        public override decimal ApplyTo(IOrder order)
        {
            throw new NotImplementedException();
        }
    }
}
