﻿using ShoppingCart.Models;
using System.Linq;

namespace ShoppingCart.Discounts.Rules
{
    public class SingleDiscountRule : IDiscountApplicationRule
    {
        public bool CheckAgainst(IOrder order)
        {
            return !order.Discounts.Any();
        }
    }
}
