﻿using ShoppingCart.Models;

namespace ShoppingCart.Discounts.Rules
{
    /// <summary>
    /// Defines a rule used to check if an <see cref="IDiscount"/> can be applied to an
    /// <see cref="IDiscountable"/> item.
    /// </summary>
    public interface IDiscountApplicationRule
    {
        /// <summary>
        /// Checks if the specified rules allow a discount to be applied to the provided
        /// <see cref="IDiscountable"/> item.
        /// </summary>
        /// <param name="discountableItem">An item for which to check if a <see cref="IDiscount"/> can be applied</param>
        /// <returns>True if a containing <see cref="IDiscount"/> can be applied to the
        /// provided <see cref="IDiscountable"/> item</returns>
        bool CheckAgainst(IOrder order);
    }
}
