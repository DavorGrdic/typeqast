﻿using ShoppingCart.Discounts.Rules;
using ShoppingCart.Models;

namespace ShoppingCart.Discounts
{
    /// <summary>
    /// Defines base operations of a discount that can be applied to an order or a part of an order.
    /// </summary>
    public interface IDiscount
    {
        #region Properties
        /// <summary>
        /// Gets or sets the unique ID of the discount.
        /// </summary>
        int ID { get; }

        /// <summary>
        /// Gets or sets the name of the discount.
        /// </summary>
        string Name { get; set; }

        /// <summary>
        /// Gets or sets the rule that determines the circumstances under whitch the
        /// discount can be applied.
        /// </summary>
        IDiscountApplicationRule ApplicationRule { get; set; }
        #endregion

        #region Methods
        /// <summary>
        /// Applies the referenced dicount to the specified <paramref name="order"/>.
        /// </summary>
        /// <param name="order">The <see cref="IOrder"/> item to which the discount will be applied</param>
        /// <returns>Discounted amount</returns>
        decimal ApplyTo(IOrder order);
        #endregion
    }
}
