﻿using ShoppingCart.Discounts;
using ShoppingCart.Models;
using System;

namespace ShoppingCart.Processors
{
    /// <summary>
    /// Handles adding and removing product and order discounts in a particular order.
    /// </summary>
    public interface IDiscountStore
    {
        #region Properties
        /// <summary>
        /// Gets or sets the <see cref="IOrder"/> on which the <see cref="IDiscountStore"/> operates.
        /// </summary>
        IOrder Order { get; }
        #endregion

        #region Methods
        /// <summary>
        /// Adds a discount to the order.
        /// </summary>
        /// <param name="discount">The discount to add</param>
        /// <returns>Reference to itself</returns>
        IDiscountStore Add(IDiscount discount);
        #endregion

        #region Events
        /// <summary>
        /// Fires when a discount was successfully applied.
        /// </summary>
        event EventHandler<DiscountEventArgs> DiscountAdded;

        /// <summary>
        /// Fires when a discount was rejected and was not applied.
        /// </summary>
        event EventHandler<DiscountEventArgs> DiscountRejected;
        #endregion
    }
}
