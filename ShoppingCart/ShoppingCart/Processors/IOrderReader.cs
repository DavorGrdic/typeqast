﻿using ShoppingCart.Models;
using System;

namespace ShoppingCart.Processors
{
    /// <summary>
    /// Reads a created order.
    /// </summary>
    public interface IOrderReader
    {
        #region Properties
        /// <summary>
        /// Gets the created order in its entirety.
        /// </summary>
        IOrder Order { get; }

        /// <summary>
        /// Gets the total number of items in the order.
        /// </summary>
        int ItemsInOrder { get; }

        /// <summary>
        /// Gets the total price of the order.
        /// </summary>
        decimal Total { get; }
        #endregion

        #region Events
        /// <summary>
        /// Fires when the <see cref="Total"/> amount is requested.
        /// </summary>
        event EventHandler<OrderEventArgs> TotalRequested;
        #endregion
    }
}
