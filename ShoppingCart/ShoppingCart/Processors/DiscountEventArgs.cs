﻿using ShoppingCart.Discounts;
using ShoppingCart.Models;
using System;

namespace ShoppingCart.Processors
{
    /// <summary>
    /// Event arguments used when an <see cref="IDiscount"/> related oepration is performed.
    /// </summary>
    public class DiscountEventArgs : EventArgs
    {
        #region Constructors
        /// <summary>
        /// Creates a new instance of the <see cref="DiscountEventArgs"/> class.
        /// </summary>
        /// <param name="discount">Used discount</param>
        public DiscountEventArgs(IDiscount discount)
        {
            Discount = discount ?? throw new ArgumentNullException(nameof(discount));
        }
        #endregion

        #region Properties
        /// <summary>
        /// Gets the discount information.
        /// </summary>
        public IDiscount Discount { get; private set; }
        #endregion
    }
}
