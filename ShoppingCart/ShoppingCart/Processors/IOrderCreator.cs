﻿using ShoppingCart.Models;
using System;

namespace ShoppingCart.Processors
{
    /// <summary>
    /// Defines an object that can modify an <see cref="IOrder"/>.
    /// </summary>
    public interface IOrderCreator
    {
        #region Properties
        IOrder Order { set; }
        #endregion

        #region Methods
        /// <summary>
        /// Adds a single item to the order.
        /// </summary>
        /// <param name="product">The item to add to the order</param>
        /// <returns>Reference to the <see cref="IOrderCreator"/> that added the item</returns>
        IOrderCreator Add(IProduct product);

        /// <summary>
        /// Adds multiple items to the order.
        /// </summary>
        /// <param name="product">The item to add to the order</param>
        /// <param name="quantity">Number of items to add</param>
        /// <returns>Reference to the <see cref="IOrderCreator"/> that added the item</returns>
        IOrderCreator Add(IProduct product, int quantity);
        #endregion

        #region Events
        /// <summary>
        /// Fires when an item is added to the order.
        /// </summary>
        event EventHandler<OrderItemEventArgs> ProductAdded;

        /// <summary>
        /// Fires when an item is modified.
        /// </summary>
        event EventHandler<OrderItemEventArgs> ProductModified;
        #endregion
    }
}
