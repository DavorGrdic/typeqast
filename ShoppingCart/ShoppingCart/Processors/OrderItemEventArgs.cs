﻿using ShoppingCart.Models;
using System;

namespace ShoppingCart.Processors
{
    /// <summary>
    /// Contains information on events that involve modification of order items.
    /// </summary>
    public class OrderItemEventArgs : EventArgs
    {
        /// <summary>
        /// Creates a new instance of the <see cref="OrderItemEventArgs"/> class.
        /// </summary>
        /// <param name="orderItem">The item that was involved in a modification event</param>
        public OrderItemEventArgs(IOrderItem orderItem)
        {
            Item = orderItem ?? throw new ArgumentNullException(nameof(orderItem));
        }

        /// <summary>
        /// Gets the order item that was added, removed or modified.
        /// </summary>
        public IOrderItem Item { get; private set; }
    }
}
