﻿using ShoppingCart.Models;
using System;

namespace ShoppingCart.Processors
{
    /// <summary>
    /// Contains information on events that involve modification of an order.
    /// </summary>
    public class OrderEventArgs : EventArgs
    {
        /// <summary>
        /// Creates a new instance of the <see cref="OrderEventArgs"/> class.
        /// </summary>
        /// <param name="order">The modified order</param>
        public OrderEventArgs(IOrder order)
        {
            Order = order ?? throw new ArgumentNullException(nameof(order));
        }

        /// <summary>
        /// Gets the modified order information.
        /// </summary>
        public IOrder Order { get; private set; }
    }
}
