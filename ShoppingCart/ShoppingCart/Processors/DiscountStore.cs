﻿using System;
using System.Linq;
using ShoppingCart.Discounts;
using ShoppingCart.Models;

namespace ShoppingCart.Processors
{
    public class DiscountStore : IDiscountStore
    {
        #region Properties
        public IOrder Order { get; private set; }
        #endregion

        #region Events
        public event EventHandler<DiscountEventArgs> DiscountAdded;
        public event EventHandler<DiscountEventArgs> DiscountRejected;
        #endregion

        #region Constructors
        public DiscountStore(IOrder order)
        {
            Order = order ?? throw new ArgumentNullException(nameof(order));
        }
        #endregion

        #region Methods
        public virtual IDiscountStore Add(IDiscount discount)
        {
            DiscountEventArgs eventArgs = new DiscountEventArgs(discount);
            if (discount.ApplicationRule.CheckAgainst(Order))
            {
                Order.Discounts.Add(discount);
                DiscountAdded?.Invoke(this, eventArgs);
            }
            else
            {
                DiscountRejected?.Invoke(this, eventArgs);
            }
            return this;
        }
        #endregion
    }
}
