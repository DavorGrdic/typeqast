﻿namespace ShoppingCart.Models
{
    /// <summary>
    /// Represents an item that can be ordered via a shopping cart.
    /// </summary>
    public interface IOrderable
    {
        /// <summary>
        /// Gets the unique identifier of the orderable item.
        /// </summary>
        int ID { get; }

        /// <summary>
        /// Gets the total price of the <see cref="IOrderable"/> item.
        /// </summary>
        decimal Total { get; }
    }
}
