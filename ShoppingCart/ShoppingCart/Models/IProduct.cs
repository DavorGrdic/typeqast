﻿namespace ShoppingCart.Models
{
    /// <summary>
    /// Defines necessary members of a product that can be added to the cart.
    /// </summary>
    public interface IProduct
    {
        /// <summary>
        /// Gets or sets the unique ID of the product.
        /// </summary>
        int ID { get; }

        /// <summary>
        /// Gets the name of the orderable item.
        /// </summary>
        string Name { get; }

        /// <summary>
        /// Gets or sets the base price of the product.
        /// </summary>
        decimal Price { get; }
    }
}
