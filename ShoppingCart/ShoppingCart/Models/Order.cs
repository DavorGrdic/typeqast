﻿using System;
using System.Collections.Generic;
using System.Linq;
using ShoppingCart.Discounts;

namespace ShoppingCart.Models
{
    public class Order : IOrder
    {
        public virtual ICollection<IOrderItem> Items { get; private set; } = new List<IOrderItem>();

        public virtual ICollection<IDiscount> Discounts { get; private set; } = new List<IDiscount>();

        public virtual decimal TotalDiscount => Math.Min(Subtotal, Discounts.Sum(d => d.ApplyTo(this)));

        public virtual int ID { get; private set; }

        public virtual decimal Total => Subtotal - TotalDiscount;

        public virtual decimal Subtotal => Items.Sum(i => i.Total);
    }
}
