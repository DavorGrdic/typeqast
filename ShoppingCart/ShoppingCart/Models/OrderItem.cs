﻿using System;

namespace ShoppingCart.Models
{
    internal class OrderItem : IOrderItem
    {
        #region Constructors
        public OrderItem(IOrder order, IProduct product) : this(order, product, 1)
        {
            
        }

        public OrderItem(IOrder order, IProduct product, int quantity)
        {
            Order = order ?? throw new ArgumentNullException(nameof(order));
            Product = product ?? throw new ArgumentNullException(nameof(product));
            if (quantity <= 0)
            {
                throw new ArgumentOutOfRangeException(nameof(quantity));
            }
            Quantity = quantity;
        }
        #endregion

        #region Properties
        public IOrder Order { get; private set; }

        public IProduct Product { get; private set; }

        public int Quantity { get; set; }

        public int ID => Product.ID;

        public decimal Total => Quantity * Product.Price;
        #endregion
    }
}
