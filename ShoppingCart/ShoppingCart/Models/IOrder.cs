﻿using ShoppingCart.Discounts;
using System.Collections.Generic;

namespace ShoppingCart.Models
{
    /// <summary>
    /// Represents an order that contains all information about ordered items,
    /// applied discounts, shipping costs, etc.
    /// </summary>
    public interface IOrder : IOrderable
    {
        /// <summary>
        /// Gets the collection of discounts applied to the reference <see cref="IDiscountable"/> item.
        /// </summary>
        ICollection<IDiscount> Discounts { get; }

        /// <summary>
        /// Gets the discount amount.
        /// </summary>
        decimal TotalDiscount { get; }

        /// <summary>
        /// Gets the collection of items added to the order.
        /// </summary>
        ICollection<IOrderItem> Items { get; }

        /// <summary>
        /// Gets the subtotal price (before discounts).
        /// </summary>
        decimal Subtotal { get; }
    }
}
