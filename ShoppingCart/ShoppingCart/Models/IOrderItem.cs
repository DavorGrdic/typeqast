﻿namespace ShoppingCart.Models
{
    /// <summary>
    /// Defines an item that is part of an order.
    /// </summary>
    public interface IOrderItem : IOrderable
    {
        /// <summary>
        /// Gets the order the referenced item belongs to.
        /// </summary>
        IOrder Order { get; }

        /// <summary>
        /// Gets the product.
        /// </summary>
        IProduct Product { get; }

        /// <summary>
        /// Gets or sets the product quantity of the referenced <see cref="IOrderItem"/>
        /// </summary>
        int Quantity { get; set; }
    }
}
