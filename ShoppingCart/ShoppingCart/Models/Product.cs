﻿namespace ShoppingCart.Models
{
    /// <summary>
    /// Represents a product.
    /// </summary>
    public class Product : IProduct
    {
        /// <summary>
        /// Gets or sets the unique ID of the product.
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Gets or sets the name of the product.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets the base price of the product.
        /// </summary>
        public decimal Price { get; set; }
    }
}
